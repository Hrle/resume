// TODO: find better solution

const Module = require("module");
(function (moduleWrapCopy) {
  Module.wrap = function (script) {
    script = script.replaceAll(
      "appPublic: resolveApp('public')",
      "appPublic: resolveApp('static')"
    );
    script = script.replaceAll(
      "appHtml: resolveApp('public/index.html')",
      "appHtml: resolveApp('static/index.html')"
    );

    return moduleWrapCopy(script);
  };
})(Module.wrap);